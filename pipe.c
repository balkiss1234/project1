#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

char* const options[] = {"ls -l", "sort", "wc", NULL};

int nbopt = 2;
pid_t pid_pere;
pid_t pid;
int status;
int pipefd1[2];
int pipefd2[2];
int fdsortie; //file descriptor du fichier de sortie

int main(int argc, char** argv) {

    pid_pere = getpid(); //pid du processus p�re
    int n; //Indice
    fdsortie = open("/home/alexandre/Bureau/blabla.txt", O_WRONLY); //ouverture du fichier texte

    if (pipe(pipefd1) == -1) { //Cr�ation du pipe
        perror("pipe"); //Si erreur...
        exit(EXIT_FAILURE);
    }

    pid = fork();
    if (pid == -1) { //Si erreur...
        perror("fork");
        exit(EXIT_FAILURE);
    } else if (pid == 0) { //Processus fils
        close(pipefd1[0]); //fermeture de la lecture
        dup2(pipefd1[1], 1); //Connexion de la sortie
        close(pipefd1[1]);

        char* const arg[] = {"ls -l", NULL};
        if (execve("/bin/ls", arg, NULL) == -1) {
            perror("execve");
        }

    } else { //Processus p�re
        close(pipefd1[1]); //fermeture de l'�criture
        dup2(pipefd1[0], 0);
        close(pipefd1[0]);
        wait(&status);

    }

    if (pipe(pipefd2) == -1) { //Cr�ation du pipe
        perror("pipe"); //Si erreur...
        exit(EXIT_FAILURE);
    }

    for (n = 0; n < nbopt; n++) { //Tant que toutes les options n'ont pas �t� parcourues
        if (getpid() == pid_pere) { //Si on est dans le p�re, on ex�cute le fork()
            pid = fork();
        }

        if (pid == -1) { //Si erreur...
            perror("fork");
            exit(EXIT_FAILURE);
        } else if (pid == 0) { //processus fils----------------------------------------------------
            close(pipefd2[1]);
            dup2(pipefd2[0], pipefd1[0]);
            close(pipefd2[0]);

            printf("fils %d, mon pid est %d\n", n, getpid());
            printf("commande : %s\n", options[n+1]);

            if (execlp(options[n + 1], options[n + 1], (char *) 0) == -1) {
                perror("execlp");
            }
        } else { //processus p�re-------------------------------------------------------------------------
            close(pipefd2[0]); //fermeture de la lecture
            dup2(pipefd2[1], 1);
            close(pipefd2[1]);
            wait(&status);

            printf("Processus fils %d termin�\n", n);
            printf("Mon pid est %d\n", getpid());
        }
    }
    return (EXIT_SUCCESS);
}
